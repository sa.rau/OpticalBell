/***************************************************************************************************

 Autor:     Stephan Rau

 Propose:   Recognize bell signal, send info to PC and start blinking

 History:
    02.11.2014 - Stephan Rau:
        o initial version
    20.12.2016 - Stephan Rau_
        o updated to use RGB LED for showing status of board

 **************************************************************************************************/

// pin names ---------------------------------------------------------------------------------------
const int BELL_SIG_IN       =  5; // bell signal input with cap and LED on board
const int PWR_UP_SIG_OUT    =  4; // red   LED of RGB
const int CONNECT_SIG_OUT   =  3; // green LED of RGB 
const int BLINK_SIG_OUT     =  2; // blue  LED of RGB
const int BELL_SIG_READ_OUT = 13;	// connected onboard LED

// serial strings ----------------------------------------------------------------------------------
const String ENABLE_CMD     = "on";     // enable blink signal
const String DISABLE_CMD    = "off";    // disable blink signal  
const String HELLO_CMD      = "hello";  // response to this command with ...
const String HERE_I_AM      = "here";   // to recognize correct com port 
const String BYE_CMD        = "bye";    // change from connection state to just power up state
const String BELL_SIG       = "bell";   // string for recognized bell signal
const char   CMD_END        = '\n';     // character for command end

// time settings -----------------------------------------------------------------------------------
const int SLOW_BLINK        = 1000;     // 1s high/low period for signal LED
const int BELL_PERIOD       = 250;      // send every 50ms that a bell happend
const int FAST_BLINK        = 100;      // 0.1 high/low Period for com port recognized

// global variables --------------------------------------------------------------------------------
int  blinkState             = LOW;
int  connectionState        = LOW;
String serialInStr;                     // read string from serial port


/*** FUNCTIONS ************************************************************************************/

// the setup function runs once when you press reset or power the board
void setup() {
  pinMode( BELL_SIG_IN      , INPUT );
  pinMode( PWR_UP_SIG_OUT   , OUTPUT);
  pinMode( CONNECT_SIG_OUT  , OUTPUT);
  pinMode( BLINK_SIG_OUT    , OUTPUT);
  pinMode( BELL_SIG_READ_OUT, OUTPUT);
  Serial.begin(9600);
  digitalWrite( PWR_UP_SIG_OUT,  HIGH ); // indicate that arduino is on
}

// the loop function runs over and over again forever
void loop() {
  
  // read bell signal
  int bellSigState = digitalRead(BELL_SIG_IN);
  
  // show read value on onboard LED, write bell signal to pc and enbale blink state
  digitalWrite( BELL_SIG_READ_OUT, bellSigState );
  if( bellSigState == HIGH ) {
    if( Serial.available() <= 0 ) { 		// make sure no command is send from PC
      //Serial.println(bellSigState);
      Serial.println(BELL_SIG); delay( BELL_PERIOD );
    }
    digitalWrite( BLINK_SIG_OUT, LOW );
    digitalWrite( CONNECT_SIG_OUT, LOW);
    blinkState = HIGH;
  }

  // create blink signal
  if( bellSigState == LOW && blinkState == HIGH ) {
    digitalWrite( BLINK_SIG_OUT, HIGH ); delay( SLOW_BLINK );
    digitalWrite( BLINK_SIG_OUT, LOW );  delay( SLOW_BLINK );
  }
  
  // read command from PC
  if( Serial.available() > 0 ) {                // check if PC has send a command
    serialInStr = Serial.readStringUntil(CMD_END);
    
    // disable blinking
    if( serialInStr == DISABLE_CMD ) {
      blinkState = LOW;
      digitalWrite( BLINK_SIG_OUT,   LOW  ); 	// make sure led is off
      digitalWrite( CONNECT_SIG_OUT, HIGH ); // show that pc connection is established      
    }

    // enable blinking
    if( serialInStr == ENABLE_CMD ) {
      digitalWrite( CONNECT_SIG_OUT, LOW );
      blinkState = HIGH;
    }
    
    // answer to hello cmd
    if( serialInStr == HELLO_CMD ) {
      Serial.println(HERE_I_AM);      
      digitalWrite( PWR_UP_SIG_OUT,  HIGH ); delay( FAST_BLINK );
      digitalWrite( PWR_UP_SIG_OUT,  LOW  ); delay( FAST_BLINK );
      digitalWrite( PWR_UP_SIG_OUT,  HIGH ); delay( FAST_BLINK );
      digitalWrite( PWR_UP_SIG_OUT,  LOW  ); delay( FAST_BLINK );
      digitalWrite( PWR_UP_SIG_OUT,  HIGH ); delay( FAST_BLINK );
      digitalWrite( PWR_UP_SIG_OUT,  LOW  ); delay( FAST_BLINK );
      digitalWrite( CONNECT_SIG_OUT, HIGH ); // show that pc connection is established
    }

    // answer to bye cmd
    if( serialInStr == BYE_CMD ) {
      digitalWrite( CONNECT_SIG_OUT, HIGH ); delay( FAST_BLINK );
      digitalWrite( CONNECT_SIG_OUT, LOW  ); delay( FAST_BLINK );
      digitalWrite( CONNECT_SIG_OUT, HIGH ); delay( FAST_BLINK );
      digitalWrite( CONNECT_SIG_OUT, LOW  ); delay( FAST_BLINK );
      digitalWrite( CONNECT_SIG_OUT, HIGH ); delay( FAST_BLINK );
      digitalWrite( CONNECT_SIG_OUT, LOW  ); delay( FAST_BLINK );
      digitalWrite( PWR_UP_SIG_OUT,  HIGH ); // show that connection was closed
    }
    
  }

}
