package org.jw.de.muc_harlaching;

import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicBoolean;
import jssc.SerialPort;
import jssc.SerialPortEvent;
import jssc.SerialPortEventListener;
import jssc.SerialPortException;

/**
 * Liest Daten vom seriellen Port und ruft einen Listener auf, sobald ein
 * vollstaendiges Kommando empfangen wurde.
 * 
 * @author Jan Breddin
 */
public class Receiver implements SerialPortEventListener  {

    private final static byte CMD_END = 10;
    private final static int DISABLE_FLASHING_LED_AFTER_SECONDS = 20;
    
    private final SerialPort _port;
    private final CountDownLatch _hereReceived;
    private final AtomicBoolean _showSignalOnPC;
    private final AtomicBoolean _isTimerRunning = new AtomicBoolean(false);
    
    private final byte[] _inputBuffer = new byte[1024];
    private int _inputByteCnt = 0;
    
    private final Timer _timer = new Timer();
    
    
    public Receiver(SerialPort port, CountDownLatch hereReceived, AtomicBoolean showSignalOnPC) {
        _port = port;
        _hereReceived = hereReceived;
        _showSignalOnPC = showSignalOnPC;
    }
    
    @Override
    public void serialEvent(SerialPortEvent spe) {
        
        if ( !spe.isRXCHAR() && !spe.isRXFLAG() ) {
            // Keine Daten, irgendein anderes Ereignis
            return;
        }
        
        try {
            final byte[] data = _port.readBytes();
            if ( data == null ) {
                // Waren wohl doch keine Daten da ...
                return;
            }
            // Pruefung auf Pufferueberlauf sparen wir uns mal ...
            System.arraycopy(data, 0, _inputBuffer, _inputByteCnt, data.length);
            _inputByteCnt += data.length;
        } catch (SerialPortException ex) {
            // TODO: Irgendwas schlaues machen! Duerfte eigentlich nicht passieren.
        }
        
        boolean cmdFound;
        do {
            cmdFound = false;
            for ( int i=0; i<_inputByteCnt && !cmdFound; ++i ) {
                if ( _inputBuffer[i] == CMD_END ) {
                    commandFound();
                    cmdFound = true;
                    final int newLength = _inputByteCnt-i-1;
                    System.arraycopy(_inputBuffer, i+1, _inputBuffer, 0, newLength);
                    _inputByteCnt = newLength;
                }
            }
        } while ( cmdFound );
        
    }
    
    private void commandFound() {
        if ( compareArrays(_inputBuffer, Commands.BELL, Commands.BELL.length) ) {
            if ( _showSignalOnPC.get() ) {
                Knocking.popUp(_port);
            } else {
                if ( !_isTimerRunning.getAndSet(true) ) {
                    // Nach DISABLE_FLASHING_LED_AFTER_SECONDS Sekunden blinken ausschalten
                    _timer.schedule(
                            new TimerTask() {
                                @Override
                                public void run() {
                                    _isTimerRunning.set(false);
                                    try {
                                        _port.writeBytes(Commands.DISABLE_CMD);
                                    } catch (SerialPortException ex) {
                                        // TODO: Hier irgendwas schlaues machen!
                                    }
                                }
                            }, 
                            DISABLE_FLASHING_LED_AFTER_SECONDS * 1000);
                }
            }
        } else if ( compareArrays(_inputBuffer, Commands.HERE, Commands.HERE.length) ) {
            _hereReceived.countDown();
        }
    }
    
    static private boolean compareArrays(byte[] a1, byte[] a2, int cnt) {
        if ( a1.length < cnt || a2.length < cnt ) {
            return false;
        }
        for ( int i=0; i<cnt; ++i ) {
            if ( a1[i] != a2[i] ) {
                return false;
            }
        }
        return true;
    }
}
