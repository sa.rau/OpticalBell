package org.jw.de.muc_harlaching;

/**
 * Konstanten fuer die Kommunikation
 * 
 * @author Jan Breddin
 */
public interface Commands {
    
    final static byte[] HELLO       = new byte[]{'h', 'e', 'l', 'l', 'o'};
    final static byte[] HERE        = new byte[]{'h', 'e', 'r', 'e'};
    final static byte[] BELL        = new byte[]{'b', 'e', 'l', 'l'};
    final static byte[] DISABLE_CMD = new byte[]{'o', 'f', 'f'};
    final static byte[] BYE         = new byte[]{'b', 'y', 'e'};
}
