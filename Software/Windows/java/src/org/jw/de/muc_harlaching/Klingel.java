package org.jw.de.muc_harlaching;

import java.lang.reflect.InvocationTargetException;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import javax.swing.UnsupportedLookAndFeelException;
import jssc.SerialPort;
import jssc.SerialPortException;
import jssc.SerialPortList;

/**
 *
 * @author Jan Breddin
 */
public class Klingel {

    private static final AtomicBoolean showSignalOnPC = new AtomicBoolean(true);
    private static StatusFrame statusFrame = null;
        
    public static void main(String[] args) throws InterruptedException, InvocationTargetException {
        
        try {
            javax.swing.UIManager.setLookAndFeel(javax.swing.UIManager.getSystemLookAndFeelClassName());
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException ex) {
            // Dann halt nicht ...
        }
        java.awt.EventQueue.invokeAndWait(() -> {
            statusFrame = new StatusFrame(showSignalOnPC);
            statusFrame.setVisible(true);
        });

        boolean isPortOk = false;
        while(!isPortOk) {
            final String[] ports = SerialPortList.getPortNames();
            for ( int i=0; i<ports.length && !isPortOk; ++i) {
                final SerialPort serialPort = new SerialPort(ports[i]);
                try {
                    final CountDownLatch hereReceived = new CountDownLatch(1);
                    Receiver cmdReceiver = new Receiver(serialPort, hereReceived, showSignalOnPC);
                    serialPort.openPort();
                    System.out.println("Port opened");
                    serialPort.addEventListener(cmdReceiver, SerialPort.MASK_RXCHAR + SerialPort.MASK_RXFLAG);
                    serialPort.setParams(
                            SerialPort.BAUDRATE_9600,
                            SerialPort.DATABITS_8,
                            SerialPort.STOPBITS_1,
                            SerialPort.PARITY_NONE); 
                    Thread.sleep(2000); // Braucht Arduino anscheinend, sonst reagiert er nicht aufs 'hello'.
                    System.out.println("Port configured");
                    if ( serialPort.writeBytes(Commands.HELLO) ) {
                        System.out.println("'hello' sent, waiting for 'here' ...");
                        try {
                            isPortOk = hereReceived.await(5000, TimeUnit.MILLISECONDS);
                            if ( isPortOk ) {
                                System.out.println("'here' received");
                            } else {
                                System.err.println("No 'here' received.");
                            }
                        } catch (InterruptedException ex) {
                            System.exit(1);
                        }
                    } else {
                        System.err.println("Failed to write hello");
                    }
                    if ( isPortOk ) {
                        // Falls LED Programm beendet wurde waehrend es noch geklingelt hat ...
                        serialPort.writeBytes(Commands.DISABLE_CMD);
                        java.awt.EventQueue.invokeLater(() -> {
                            statusFrame.setConnected(true);
                        });
                        // Abmelden bei Programmende von Arduino
                        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
                            try {
                                serialPort.writeBytes(Commands.BYE);
                                System.out.println("'bye' sent.");
                            } catch (SerialPortException ex) {
                                // Abmeldung fehlgeschlagen. Kann man jetzt auch nichts machen ...
                            }
                        }, "time-to-say-goodby-thread"));
                    } else {
                        serialPort.closePort();
                    }
                } catch (SerialPortException ex) { 
                    // War wohl nichts. Naechsten Port probieren.
                }
                Thread.sleep(1000);
            }
        }
    }
}
